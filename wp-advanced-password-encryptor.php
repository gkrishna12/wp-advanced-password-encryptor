<?php
/*
Plugin Name: WP Advanced Password Encryptor
Description: Replaces the pluggable wordpress function wp_hash_password() and wp_check_password(). !!! Note - All users must reset their passwords after activating this plugin, their old passwords will not work. !!!
Author: K Gopal Krishna
Version: 1.0.0
Author URI: https://kgopalkrishna.com/
*/

require('includes/fernet.php');

if( !function_exists('wp_hash_password') ) {
	function wp_hash_password($password) {

		$salt = 'u27-i9haTauYpCvDQm1uCfzBEujtxWY6';
		$key = 'o__v6qTlIGDJqj9HWxE95H9s1yz6hdag--HMm8NPGD4=';
		$fernet = new Fernet($key);
		
		$password = hash('sha256', $password.$salt);
		$password = $fernet->encode($password);

		return $password;
	}
}


if( !function_exists('wp_check_password') ) {
	function wp_check_password($password, $hash, $user_id = '') {

		$salt = 'u27-i9haTauYpCvDQm1uCfzBEujtxWY6';
		$key = 'o__v6qTlIGDJqj9HWxE95H9s1yz6hdag--HMm8NPGD4=';
		$fernet = new Fernet($key);
		
		$hash = $fernet->decode($hash);
		$password = hash('sha256', $password.$salt);

		return ($password == $hash)
	}
}

