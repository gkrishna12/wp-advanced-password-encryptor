# README #

### What is this repository for? ###

This is a wordpress plugin developed to make your site's password protection more secure.

* Plugin Name: WP Advanced Password Encryptor
* Description: Replaces the pluggable wordpress function wp_hash_password() and wp_check_password(). !!! Note - All users must reset their passwords after activating this plugin, their old passwords will not work. !!!
* Author: K Gopal Krishna
* Version: 1.0.0
* Author URI: https://kgopalkrishna.com/

### How do I get set up? ###

* Download the zip.
* Go to wordpress dashboard.
* Go to Plugins -> Add New -> Upload Plugin
* Upload the zip file.
* Finally Activate the plugin.

That's it. All you need to do is reset all user's passwords, since the old password won't authenticate the users.

### Who do I talk to? ###

* K Gopal Krishna - mail@kgopalkrishna.com